# -*- mode: ruby -*-

require 'yaml'

nodes = YAML.load_file('nodes.yaml')

Vagrant.configure('2') do |config|
    config.vm.box = 'centos/7'

    nodes.keys.each do |node|
        node_settings = nodes[node]["settings"]

        node_hostname = node_settings['hostname']

        config.vm.define "#{node_hostname}" do |db|
            db.vm.hostname = node_hostname

            db.vm.provider 'virtualbox' do |vbox|
                vbox.cpus = node_settings["cpus"] ? node_settings["cpus"] : 1
                vbox.memory = node_settings["memory"] ? node_settings["memory"] : 1024
            end

            addresses = node_settings["addresses"].length
            
            (0 .. addresses-1).each do |address_index|
                db.vm.network "private_network",
                    ip: node_settings["addresses"][address_index]["ip"],
                    netmask: node_settings["addresses"][address_index]["netmask"]
            end
        end
    end

    config.vm.provision "shell", inline: <<-EOF
        yum update -y
        yum install -y wget
        cp -r /home/vagrant/.ssh /root
    EOF
end

# vi: ft=ruby :
# vi: tabstop=4 shiftwidth=4 expandtab :