#!/usr/bin/ruby
# -*- mode: ruby -*-

project_dir = File.expand_path(File.dirname(__FILE__))

file_cache_path   project_dir + '/cache'
cookbook_path     [ "#{project_dir}/cookbooks", "#{project_dir}/berks-cookbooks" ]
role_path         project_dir + '/roles'

# vi: ft=ruby :
# vi: tabstop=4 shiftwidth=4 expandtab :