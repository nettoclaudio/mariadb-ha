name 'database-proxy'

description 'Common configurations for all database proxy servers.'

run_list *[
    'recipe[mariadb-ha::prepare_node]',
    'recipe[mariadb-ha::maxscale]',
    'recipe[mariadb-ha::database-proxy_cluster_create]'
]

default_attributes(
    'timezone' => 'America/Sao_Paulo',

    'pacemaker' => {
        'corosync' => {
            'cluster_name' => 'DatabaseProxyCluster',

            'nodes' => {
                'proxy01.cluster.tld' => '172.28.192.10',
                'proxy02.cluster.tld' => '172.28.192.11'
            }
        },

        'pcs' => {
            'password' => 'iwillchageit(:'
        }
    }
)
