name 'database'

description 'Common configurations for all database servers.'

run_list *[
    'recipe[basic]',
    'recipe[mariadb::server]',
    'recipe[database]'
]

default_attributes(
    'mariadb' => {
        'use_default_repository' => true,

        'install' => {
            'version' => '10.2'
        },

        'mysqld' => {
            'bind_address' => '0.0.0.0'
        },

        'server_root_password' => 'my-awesome-password-from-root-user',

        'replication' => {
            'master' => 'db01.cluster.tld',
            'user' => 'replication_user',
            'password' => 'f0abcfab-2a73-40f8-81d9-f3b4d7e94805'
        }
    }
)
