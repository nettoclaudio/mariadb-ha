name             'mariadb-ha'
maintainer       'Claudio Netto'
maintainer_email 'nettoclaudio@ufrj.br'
license          'Apache 2.0'
description      'Installs/Configures MariaDB HA'
long_description ''
version          '1.0.0'
source_url       ''
issues_url       ''

depends 'hostsfile'
depends 'mariadb'

supports 'centos', '>= 7'
