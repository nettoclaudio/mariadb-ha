package 'tzdata'
package 'vim'
package 'wget'

timezone = node['timezone'] || 'UTC'

execute 'set-timezone' do
    command "timedatectl set-timezone '#{timezone}'"

    not_if "timedatectl status | grep '#{timezone}'"
end

node['peers'].each do |node_name, ip_addr|
    hostsfile_entry ip_addr do
        hostname node_name
        comment 'Update by Chef'
    
        action :create_if_missing
    end
end
