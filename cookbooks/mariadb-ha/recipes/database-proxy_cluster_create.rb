execute 'Setup authentication tokens for pcs command' do
  command "pcs cluster auth -u hacluster -p '#{node['pacemaker']['pcs']['password']}' #{node['pacemaker']['corosync']['nodes'].keys.join(' ')}"
  sensitive false
  creates '/var/lib/pcsd/pcs_user.conf'
  creates '/var/lib/pcsd/tokens'
end

execute 'Create and start cluster-engine (corosync)' do
    command "pcs cluster setup --start --name #{node['pacemaker']['corosync']['cluster_name']} #{node['pacemaker']['corosync']['nodes'].keys.join(' ')} && sleep 2"
    creates '/etc/corosync/corosync.conf'
end

execute 'Verify cluster-engine running' do
    command '[ $(pcs status cluster | grep -c "UNCLEAN (offline)") -eq 0 ]'
    retries 50
    retry_delay 1
end
