server_root_password = node['mariadb']['server_root_password']

replication_user            = node['mariadb']['replication']['user']
replication_master_hostname = node['mariadb']['replication']['master']
replication_password        = node['mariadb']['replication']['password']

template '/tmp/create_user_replication.sql' do
    source 'create_user_replication.sql.erb'
    owner 'root'
    group 'root'
    mode  '0700'
    action :nothing
    sensitive true
end

execute 'crete-user-for-replication' do
    command "mysql -u root --password='#{server_root_password}' < /tmp/create_user_replication.sql"

    notifies :create, 'template[/tmp/create_user_replication.sql]', :before
    notifies :delete, 'template[/tmp/create_user_replication.sql]', :delayed

    not_if "echo \"SELECT 1 FROM mysql.user WHERE User = '#{replication_user}';\" | \
            mysql -sN -u root --password='#{server_root_password}' | \
            grep 1"

    sensitive true
end

file '/etc/my.cnf.d/30-replication.cnf' do
    action :nothing
end

cookbook_file '/etc/my.cnf.d/31-replication.cnf' do
    source 'mariadb-replication-master.cnf'
    owner 'root'
    group 'mysql'
    mode  '0640'
    action :create

    notifies :restart, 'service[mysql]', :delayed
    notifies :delete, 'file[/etc/my.cnf.d/30-replication.cnf]', :before

    not_if "test -f /etc/my.cnf.d/31-replication.cnf"
    only_if { node.name == "#{replication_master_hostname}" }
end

template '/etc/my.cnf.d/31-replication.cnf' do
    source 'mariadb-replication-slave.cnf.erb'
    owner 'root'
    group 'mysql'
    mode  '0640'
    action :create

    notifies :restart, 'service[mysql]', :delayed
    notifies :delete, 'file[/etc/my.cnf.d/30-replication.cnf]', :before

    not_if "test -f /etc/my.cnf.d/31-replication.cnf"
    not_if { node.name == "#{replication_master_hostname}" }
end

template '/tmp/mariadb-replication-slave-change-master.sql' do
    source 'mariadb-replication-slave-change-master.sql.erb'
    owner 'root'
    group 'root'
    mode  '0640'
    action :nothing

    sensitive true
end

execute 'mariadb-replication-slave-change-master' do
    command "mysql -u root --password='#{server_root_password}' < /tmp/mariadb-replication-slave-change-master.sql"

    notifies :create, 'template[/tmp/mariadb-replication-slave-change-master.sql]', :before
    notifies :delete, 'template[/tmp/mariadb-replication-slave-change-master.sql]', :delayed
    
    sensitive true

    not_if { node.name == "#{replication_master_hostname}" }
end