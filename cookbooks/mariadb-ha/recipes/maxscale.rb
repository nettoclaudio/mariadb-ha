execute 'download-maxscale-rpm' do
    command 'wget -O /tmp/maxscale.rpm https://downloads.mariadb.com/MaxScale/2.1.8/rhel/7/x86_64/maxscale-2.1.8-1.rhel.7.x86_64.rpm'
    creates '/tmp/maxscale.rpm'
    action :nothing
end

file '/tmp/maxscale.rpm' do
    action :nothing
end

execute 'install-maxscale-from-rpm' do
    command 'rpm -i /tmp/maxscale.rpm'

    notifies :run, 'execute[download-maxscale-rpm]', :before
    notifies :disable, 'service[maxscale]', :immediately
    notifies :delete, 'file[/tmp/maxscale.rpm]', :delayed

    only_if 'systemctl status maxscale.service'
end

service 'maxscale' do
    action :nothing
end

cookbook_file '/etc/maxscale.cnf' do
    source 'maxscale.cnf'
    mode   '0644'
    owner  'root'
    group  'root'
end

package 'pacemaker'
package 'corosync'
package 'pcs'

pacemaker_user_password = node['pacemaker']['pcs']['password'].crypt('$6$mariaDBHA')

user 'hacluster' do
    password "#{pacemaker_user_password}"
    action :modify
end

service 'pcsd' do
    supports restart: true, reload: true, status: true
    action [:enable, :start]
end

service 'corosync' do
    action :enable
end

service 'pacemaker' do
    action :enable
end
